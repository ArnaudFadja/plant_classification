\section{Deep Learning and Convolutional Neural Networks}
\label{dl_cnn}
ML and DL are fields of AI which study algorithms for analysing and inducing knowledge from data. They are defined as a set of methods that can automatically identify patterns in data and then use these patterns to predict future (unseen) data or to perform decision-making under uncertainty ~\cite{murphy2012machine}. Recently, a DL algorithm called CNN has shown a good ability to extract patterns from data, such as images. CNNs are considered the state of the art for solving classification problems and have been successfully applied to image classification and object detection ~\cite{druzhkov2016survey}.

%\begin{figure}[h]
%%\begin{center}
%\includegraphics[width=13cm,height=13cm,keepaspectratio]{cnn} %[scale=0.13]
%%\end{center}
%\caption{Convolutional Neural Network.}\label{fig:cnn} 
%\end{figure}


A CNN, as shown in Figure~\ref{convolution_pooling_op}, has two components: the first part, \emph{feature learning}, extracts patterns or features from the image while the second part, \emph{classification}, classifies the image based on the extracted features. The first part relies on two operations: the \emph{convolutional} operation, Figure~\ref{convolution_op} scans the image from the top to the bottom to extract possible features using a certain number of filters also called kernels. Each filter extracts a different type of feature known as convolutional kernel. After convolution, a \emph{pooling} operation, e.g. Figure ~\ref{pooling_op}, is applied to down-sample the extracted features and reduce the complexity of the representation. It also scans the features and performs an operation (generally Maximum) which summarizes a set of values into one. Those operation are often followed by an activation function, e.g the Rectified Linear Unit (ReLu), which learns the non linearity in the data.
%A function, called \emph{activation function}, e.g the Rectified Linear Unit (ReLu), is generally applied to the output of each convolution operation to introduce nonlinearity in the model.
The convolution and pooling operations are organized into layers. The first layers identify common features like corners and edges. The deeper the layer the more complex the features extracted are. The \emph{classification} portion of the network is a shallow (dense) artificial neural network also called \emph{fully connected neural network}, see Figure~\ref{fig:fullyNetwork}, in which artificial neurons are organized into layers. Each neuron receives inputs from all neurons in the previous layer, and performs a linear combination on these inputs whose result is passed through an activation function, generally ReLu. The first layer is the \emph{input layer} which represents features extracted in the convolutional part and the last layer is the \emph{output layer} yielding the predicted responses. The number of neurons in the output layer is equal to the number of classes to identify or to the number of objects to detect. Intervening layers of the classification portion are called \emph{hidden layers}. %, generally no more than two.


\begin{figure}[h]
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=5cm,height=4cm,keepaspectratio]{convolution_op}
  \caption{Convolution operation.}
  \label{convolution_op}
\end{subfigure}%
%\vspace*{4cm}
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=6cm,height=3.2cm]{pooling_op}
  \caption{Pooling operation}
    \label{pooling_op}
\end{subfigure}
  
\caption{ Convolution and pooling operations.}
 \label{convolution_pooling_op}
\end{figure}
%\url{https://towardsdatascience.com/a-comprehensive-guide-to-convolutional-neural-networks-the-eli5-way-3bd2b1164a53}

 \begin{figure}[h]
\begin{center}
\includegraphics[scale=0.29]{Example-of-fully-connected-neural-network}
\end{center}
\caption{Fully connected neural network.}\label{fig:fullyNetwork} 
\end{figure}

Once the architecture of the network is defined, it should be trained to perform a certain task. In the case of binary classification (two classes), training a CNN means repeatedly adjusting, for example by means of gradient descent ~\cite{bottou2012stochastic} and back-propagation ~\cite{phansalkar1994analysis}, both the filters in the convolutional layers and the weights in the dense layers using a set of examples called \emph{training set}. These adjustments are done such that eventually the model is able to correctly classify almost all the examples in the training set and hopefully acquires the ability to correctly classify new examples, called \emph{generalization capacity}. 

Training a neural network is also repeatedly updating its weights in order to minimize an \emph{objective or loss function}. The loss function measures how well a network behaves at each iteration. It recapitulates in a single value the differences between the expected classes of examples in the training set and the ones provided by the network. The \emph{training accuracy} of the network is the fraction of examples correctly classified with respect to the whole examples. Training a network is therefore gradually updating its weights which  progressively reduces the loss (or increases the accuracy) in order to correctly classify almost all examples in the training set. Weights are updated in order to move in the direction that minimizes the loss function. Gradient descent ~\cite{bottou2012stochastic} (GD) is an optimization algorithm that computes (at each iteration) the gradient of the loss function w.r.t each weight in the network and updates the weights with a fraction (called learning rate) of the computed gradients. In order to efficiently compute the gradients (which are often too many in a network), GD relies on one of the most efficient algorithms called back-propagation algorithm ~\cite{phansalkar1994analysis}. The algorithm, which involves the recursive application of the chain rule from calculus, was developed specifically to efficiently compute gradients during the training process of deep neural networks. It works backward from the output of the network toward the input propagating the error in the predicted output that is used to calculate gradient for each weight.

The standard gradient descent algorithm computes gradients at each iteration using all examples in the training set. If the training set is  large, the algorithm can converge very slowly. To avoid slow convergence, gradients can be computed using a single example, randomly selected in the training set. Even if in this case the algorithm can converge quickly, it is generally hard to reach high training set accuracy. A compromise often used is the mini batch stochastic gradient descent (SGD) ~\cite{khirirat2017mini}: at each iteration a mini batch of examples from the training set is randomly sampled to compute the gradient. The training process is done in different \emph{epochs}. An \emph{epoch} occurs when all examples in the training are used. The number of epochs controls the number of complete passes through the training dataset. This method usually provides fast converge and high accuracy. 


Since the construction and the training of a network rely on a set of parameters named hyper-parameters, a \emph{validation set} of examples is often useful to find the hyper-parameters values which lead to better generalization. If the model provides acceptable accuracy (or loss) on the training set and bad on the validation set, this phenomenon is called over-fitting. Many techniques are available to avoid over-fitting. One of the computationally cheapest way to regularize a deep neural network is called Dropout ~\cite{srivastava2014dropout}. Dropout works by probabilistically removing or \emph{dropping out} inputs to a layer, which may be input variables in the data sample or activations from a previous layer. It has the effect of simulating a large number of networks with very different network structure and, in turn, making nodes in the network generally more robust to the inputs. It prevents a net with lot of weights to over-fit on the training data. Other techniques to avoid over-fitting, $L_1$ and $L_2$ regularization ~\cite{goodfellow2016deep}, add another term (that depends on the weight) to the loss function in order to favor small weights ($\sim0$). Small weights can then be dropped during the evaluation of the network.
Then the accuracy of the model is assessed by running the model on a set of unseen examples called \emph{test set}.
  
\newpage
